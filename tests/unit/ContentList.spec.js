import { shallowMount } from '@vue/test-utils'
import ContentList from '@/components/content-list/ContentList'
import ContentListItem from '@/components/content-list-item/ContentListItem'


describe('ContentList', () => {
  describe('when items prop is an array of one of more objects', () => {
    let wrapper;  
    beforeEach(() => {
      const mockItems = [
        { id: 123 },
        { id: 456 },
        { id: 789 }
      ]

      wrapper = shallowMount(ContentList)
      wrapper.setProps({ items: mockItems })
    })
  
    it('should render the correct number of list items', () => {
      expect(wrapper.findAll(ContentListItem).length).toBe(3)
    })

    describe('and when item-click event is caught', () => {
      it('should update selected item', () => {
        wrapper.find(ContentListItem).vm.$emit('item-click', { id: 123 })
        expect(wrapper.vm.selected).toBe(123)
      })
    })
  })
})

describe('when items is NOT defined', () => {
  let wrapper;  
  beforeEach(() => {
    const mockItems = []

    wrapper = shallowMount(ContentList)
    wrapper.setProps({ items: mockItems })
  })

  it('should render NOT render any list items', () => {
    expect(wrapper.find(ContentListItem).exists()).toBe(false)
  })
})
