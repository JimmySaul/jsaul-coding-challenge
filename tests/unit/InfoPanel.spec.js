import { shallowMount } from '@vue/test-utils'
import InfoPanel from '@/components/info-panel/InfoPanel'

describe('InfoPanel', () => {
  describe('when item prop is set', () => {
    let wrapper;  
    beforeEach(() => {
      const mockCurrentMovieDetails = { 
        id: 123,
        title: 'Mock title',
        release_date: '2019-05-24',
        runtime: 120,
        production_countries: [
          { name: 'USA' },
          { name: 'San Marino' }
        ],
        overview: 'Mock overview'
      }
      const mockCurrentMovieCert = 'U'

      wrapper = shallowMount(InfoPanel, { 
        propsData: {
          currentMovieDetails: mockCurrentMovieDetails,
          currentMovieCert: mockCurrentMovieCert
        }
      })
    })
  
    it('should render the correct title', () => {
      expect(wrapper.html()).toContain('Mock title')
    })

    it('should render the correct list of details', () => {
      expect(wrapper.html()).toContain('120min')
      expect(wrapper.html()).toContain('2019')
      expect(wrapper.html()).toContain('USA')
      expect(wrapper.html()).toContain('San Marino')
    })

    it('should render the correct overview', () => {
      expect(wrapper.html()).toContain('Mock overview')
    })

    // it('should set the correct poster path attribute', () => {
    //   const backgroundStyle = wrapper.find('.content-list-item__card').attributes()
    //   expect(backgroundStyle.style).toEqual('background-image: url(https://image.tmdb.org/t/p/w200/123.jpg);')
    // })
  })
})
