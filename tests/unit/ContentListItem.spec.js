import { shallowMount } from '@vue/test-utils'
import ContentListItem from '@/components/content-list-item/ContentListItem'


describe('ContentListItem', () => {
  describe('when item prop is set', () => {
    let wrapper;  
    beforeEach(() => {
      const mockItem = { 
        id: 123,
        title: 'Mock title',
        poster_path: '/123.jpg'
      }

      wrapper = shallowMount(ContentListItem, { 
        propsData: {
          item: mockItem
        }
      })
    })
  
    it('should render the correct title', () => {
      expect(wrapper.html()).toContain('Mock title')
    })

    it('should set the correct poster path attribute', () => {
      const backgroundStyle = wrapper.find('.content-list-item__card').attributes()
      expect(backgroundStyle.style).toEqual('background-image: url(https://image.tmdb.org/t/p/w200/123.jpg);')
    })
  })

  describe('when item is selected', () => {
    let wrapper;  
    beforeEach(() => {
      const mockItem = { 
        id: 123,
        title: 'Mock title',
        poster_path: '/123.jpg'
      }
      wrapper = shallowMount(ContentListItem, { 
        propsData: {
          item: mockItem,
          selected: mockItem.id
        }
      })
    })
    it('should apply selected CSS class', () => {
      const listItem = wrapper.find('.content-list-item')
      expect(listItem.classes()).toContain('content-list-item--selected')
    })
  })
})
