import axios from 'axios'
import moment from 'moment'

const apiKey = "7a853a18982534324765451949fc1655";
const language = "language=en-GB";
const apiClient = axios.create({
  baseURL: "https://api.themoviedb.org/3",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

let maxDate = moment().format('YYYY-MM-DD')
let minDate = moment().subtract(7,'d').format('YYYY-MM-DD');

export default {
  getMovies() {
    return apiClient.get(`/discover/movie?api_key=${apiKey}&${language}&region=GB&primary_release_date.gte=${minDate}&primary_release_date.lte=${maxDate}&sort_by=popularity.desc`);
  },
  getMovieDetails(movieId) {
    return apiClient.get(`/movie/${movieId}?api_key=${apiKey}`)
  },
  getMovieCert(movieId) {
    return apiClient.get(`/movie/${movieId}/release_dates?api_key=${apiKey}`)
  }
};